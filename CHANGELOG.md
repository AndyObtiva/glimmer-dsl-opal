# Change Log

## 0.2.0

- Color support
- Font support
- Custom Widget Support
- Hello, Custom Widget! sample
- Updated Hello, Combo! sample to match the latest changes in Glimmer DSL for SWT
- `SWT` full re-implementation in Opal as `Glimmer::SWT` with all the `SWT` style constants

## 0.1.0

- Code redesign to better match the glimmer-dsl-swt APIs
- opal-jquery refactoring
- opal-rspec test coverage

## 0.0.9

- Upgraded to glimmer gem v0.9.3
- Fixed issue with missing Glimmer::Opal::ElementProxy#id=(value) method breaking Contact Manager sample Find feature

## 0.0.8

- Contact Manager sample support

## 0.0.7

- Tic Tac Toe sample support
- Login sample support

## 0.0.6

- Hello, Tab! sample support

## 0.0.5

- Hello, Browser! sample support

## 0.0.4

- Hello, List Single Selection! sample support
- Hello, List Multi Selection! sample support

## 0.0.3

- Hello, Computed! sample support

## 0.0.2

- Hello, Combo! sample support

## 0.0.1

- Initial support for webifying Glimmer SWT apps
- Support for Shell and Label widgets (text property only).
