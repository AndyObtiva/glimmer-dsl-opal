# TODO

Here is a list of tasks to do (moved to CHANGELOG.md once done). 

## Next

- Hello, Custom Shell! Sample

## Soon

- Implement part of net/http with jQuery for use in Glimmer apps since it is not implemented by Opal

## Issues

- Look into edit table item error on sorting table

## Tasks

- Do away with redraw everywhere possible
- Listeners
- Basic data-binding
- Radio button
- Checkbox button
- Support multiple windows as multiple tabs
- Support different themes by detecting browser OS (Mac, Windows, Linux)
- Support Document.ready? as part of Glimmer's top level keywords so clients don't need to use it
- Test and document opal-hot-reloader as an option for hot loading glimmer-dsl-opal

## Samples

- Host all internal samples online

## Production Apps

- DripChat: Distraction-Free Business Chat (paid business account) with DripComm technology to ensure chats are balanced and one cannot send too many messages without another's acknowledgment. DripComm allows no more than one short message without a reply, requiring a reply before you make another reply, thus auto-moderating itself). DripComm is short for Drip Communication. It will be open-source, but under a special license that requires a paid business account to reuse by 3rd party developers.
- DripBlog (free) with comment self moderating DripComm technology and Glimmer Blog as first customer
- FreeHire: Web app for requesting software development project services and hiring developers for free in exchange for marketing them with finished software online citation from the start. No guarantees for maintenance though. It is done freely only. 
- DripEmail (paid business account) email service using DripComm technology
- DripForum (with self moderating DripComm technology that allows no more than one message without a reply, requiring a reply before you make another reply, thus auto-moderating)
- ThoughtBarf (free) to barf thoughts online with short messages relying on self moderating DripComm technology
- PublishOrPerish (free) a site to publish articles and receive comments. Leveraged self moderating DripComm technology. 
- FriendsOnly (free): a friends only social networking site that intentionally avoids supporting family-tree and dating relationships/status. This makes it a safe place for networking with friends only. Leverages the DripComm technology
- ProfessionalOpinion: a professional question/answer website relying on self-moderating DripComm technology (free for answering and browsing but not for asking)
- indextheworld.org or cyclopedia.world (free): a publicly co-authored encyclopedia. Circumvents many issues in wikipedia by making pages available for authorship by one person only (thus eliminating the issues of different writing styles and inconsistencies per page on wikipedia). If changes were needed, other people may submit inquiries to the page author to update the page. If the author does not respond in a year and a half, the page is released to the public to be claimed by a different author. If the page author puts inaccurate info, people can complain. Once there are 8 complaints, the page is flagged for review by website volunteer moderators approved by website owner (me). They can accept the complaints or deny them. If they accept the complaints, the page author is revoked and the page becomes available to be claimed by a differnet author. If not, then the page author stays and next time needs 1.5x the complains (12 for example) to flag page for review. This auto-punishes complainers if they complain without the page having any issues as next time it becomes harder to flag it.
- Build FolkAdvice.com (free): a website for asking for and giving advice. Also relies on DripComm self-moderating technology
- Desktopify.org (free): freely convert any web app into a desktop app using Glimmer scaffolding. Provide scaffolded app as src, dmg, pkg, app, msi, and jar+script for Linux. 
- GlimmerAppStore.com: cross platform alternative to Mac App Store to host and sell glimmer apps with auto update support
- helpmeteachyou.org (free): free education platform
- social.community (free): online community gathering with DripComm self-moderating technology.
